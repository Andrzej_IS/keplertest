﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeplerCITest
{
    public class TestClass
    {
        private string text;
        private int multiplier;

        public TestClass(string t, int n)
        {
            text = t;
            multiplier = n;
        }

        public int Multiply(int a)
        {
            return a*multiplier;
        }

        public string Shout()
        {
            return text;
        }
    }
}
