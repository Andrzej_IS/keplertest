﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KeplerCITest;

namespace KeplerCITest.UnitTests
{
    [TestClass]
    public class TestClassUnitTests
    {
        [TestMethod]
        public void TestText()
        {
            var test = new TestClass("Scream", 1);
            Assert.AreEqual("Scream",test.Shout());

        }

        [TestMethod]
        public void TestMultiply()
        {
            var test = new TestClass("Scream", 1);
            Assert.AreEqual(5, test.Multiply(5));
        }
    }
}
